﻿// SBHWvectorClass.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>

class Vector
{

public:
    Vector() : x1(0), y1(0), z1(0), x2(0), y2(0), z2(0)
    {}
    Vector(int _x1, int _y1, int _z1, int _x2, int _y2, int _z2) : x1(_x1), y1(_y1), z1(_z1), x2(_x2), y2(_y2), z2(_z2)
    {}
    double vectorLength() {
        int xCoord = x2 - x1;
        int yCoord = y2 - y1;
        int zCoord = z2 - z1;
        double vectorModule = sqrt(xCoord * xCoord + yCoord * yCoord + zCoord * zCoord);
        return vectorModule;
    }
private:
    int x1, y1, z1, x2, y2, z2;
    
};
int main()
{
    Vector v(2, 3, 2, 1, 5, 7);
    std::cout << v.vectorLength();
    
}


